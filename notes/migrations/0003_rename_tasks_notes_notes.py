# Generated by Django 4.1.7 on 2023-03-16 16:31

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("notes", "0002_alter_notes_tasks"),
    ]

    operations = [
        migrations.RenameField(
            model_name="notes",
            old_name="tasks",
            new_name="notes",
        ),
    ]
