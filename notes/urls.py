from django.urls import path
from notes.views import (
    create_note,
)


urlpatterns = [
    path("create/note", create_note, name="create_note"),
]
