from django.shortcuts import render, redirect
from notes.forms import NoteForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def create_note(request):
    if request.method == "POST":
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = NoteForm()

    context = {
        "form": form,
    }
    return render(request, "notes/create_note.html", context)
