from django.urls import path
from tasks.views import (
    create_task,
    show_my_tasks,
)

urlpatterns = [
    path("create/task", create_task, name="create_task"),
    path("my_tasks/", show_my_tasks, name="show_my_tasks"),
]
