from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task
import pandas as pd
from plotly.offline import plot
import plotly.express as px


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()

    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    qs = Task.objects.all()
    tasks_data = [
        {
            "Task": x.name,
            "Start": x.start_date,
            "Finish": x.due_date,
            "Assignee": x.assignee.username,
        }
        for x in qs
    ]

    df = pd.DataFrame(tasks_data)

    fig = px.timeline(
        df, x_start="Start", x_end="Finish", y="Task", color="Assignee"
    )

    fig.update_yaxes(autorange="reversed")
    gantt_plot = plot(fig, output_type="div")
    context = {"my_tasks": tasks, "plot_div": gantt_plot}
    return render(request, "tasks/my_tasks.html", context)
