from django.urls import path
from projects.views import (
    list_projects,
    show_project,
    create_project,
    show_notes,
)
from notes.views import create_note

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("notes/<int:id>/", show_notes, name="show_notes"),
    path("create/note", create_note, name="create_note"),
]
