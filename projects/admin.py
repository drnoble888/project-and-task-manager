from django.contrib import admin
from projects.models import Project, Company


# Register your models here.
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    ()


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    ()
